package com.hust.system.quartz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WaterQuartzApplication {

    public static void main(String[] args) {
        SpringApplication.run(WaterQuartzApplication.class, args);
    }

}
