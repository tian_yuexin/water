package com.hust.system.common;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WaterCommonApplication {

    public static void main(String[] args) {
        SpringApplication.run(WaterCommonApplication.class, args);
    }

}
