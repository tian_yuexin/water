package com.hust.system.system.test.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author tlx
 * @since 2021-10-13
 */
@RestController
@RequestMapping("/test/account")
public class AccountController {

}

