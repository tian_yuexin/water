package com.hust.system.system.test.service.impl;

import com.hust.system.system.test.model.Account;
import com.hust.system.system.test.mapper.AccountMapper;
import com.hust.system.system.test.service.AccountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author tlx
 * @since 2021-10-13
 */
@Service
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account> implements AccountService {

}
