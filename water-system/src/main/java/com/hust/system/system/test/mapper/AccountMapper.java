package com.hust.system.system.test.mapper;

import com.hust.system.system.test.model.Account;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author tlx
 * @since 2021-10-13
 */
public interface AccountMapper extends BaseMapper<Account> {

}
