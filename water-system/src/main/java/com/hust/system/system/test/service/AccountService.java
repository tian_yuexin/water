package com.hust.system.system.test.service;

import com.hust.system.system.test.model.Account;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tlx
 * @since 2021-10-13
 */
public interface AccountService extends IService<Account> {

}
