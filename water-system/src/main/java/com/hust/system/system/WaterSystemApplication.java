package com.hust.system.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WaterSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(WaterSystemApplication.class, args);
    }

}
